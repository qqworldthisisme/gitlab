require "mimemagic/version"

module MimeMagic
  class Error < StandardError; end
  raise Error, 'This gem should never be required'
end
